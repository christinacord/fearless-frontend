import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


async function loadData() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  const response2 = await fetch('http://localhost:8000/api/states/');
  if (response.ok && response2.ok) {
    const data = await response.json();
    const data2 = await response2.json();
      root.render(
        <React.StrictMode>
          <App attendees={data.attendees} states={data2.states}/>
        </React.StrictMode>
      )
  } else {
      console.error(response);
  }
}
loadData();
