function createCard(name, location, description, pictureUrl, formattedEndDate, formattedStartDate) {
    return `
      <div class="card shadow p-3 mb-5 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">
    ${formattedStartDate} - ${formattedEndDate}
  </div>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.error("There was an error");
      } else {
        const data = await response.json();

        let index = 0;
        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const location = details.conference.location.name;
              const startDate = details.conference.starts;
              const endDate = details.conference.ends;
              const formattedStartDate = new Date(startDate).toLocaleDateString();
              const formattedEndDate = new Date(endDate).toLocaleDateString();
              const html = createCard(title, location, description, pictureUrl, formattedEndDate, formattedStartDate);
              const column = document.querySelector(`#col-${index % 3}`);
              column.innerHTML += html;
              index += 1;
            }
          }

      }
    } catch (e) {
        console.error("There was an error", e);
    }

  });
